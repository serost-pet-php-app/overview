# Full PHP Website Pipeline with IaC

### [CC0 - NO RIGHTS RESERVED](./LICENSE)

# Why?
This project is used as a portfolio for [me](https://gitlab.com/serost)! <br/>
This repo is the part of a whole pet [PHP Website CI/CD Pipeline](https://gitlab.com/serost-pet-php-app) project.

## Gallery
Notable stuff is located under [gallery](./gallery) directory, there you can see
images of deployed infrastructure, <br/>but to summarize, here is a
DrawIO chart of a whole infrastructure:
![alt text](./gallery/architecture.drawio.png)

## Where is the code?
Code is spread across multipe repositories under GitLab Group [link](https://gitlab.com/serost-pet-php-app).

- [Overview](https://gitlab.com/serost-pet-php-app/overview). This repository.
   <br/>Contains general information about project, goals, used technologies, gallery, etc.

- [Infrastructure](https://gitlab.com/serost-pet-php-app/infrastructure). Contains IaC code for deploying infrastructure and application to AWS.<br/> Worth noting that this may be easily transformed into full featured GitOps repository, since main paradigm is followed - "git repository as a single source of truth".

- [PHP Application](https://gitlab.com/serost-pet-php-app/php-application-root). Contains sources to a website, HTML, CSS, PHP.<br/> Also has a docker prepared environment code, that allows interactively test code, by deploying simple containerized infrastructure at localhost.<br/> Has a CI pipeline, which pushes artifact to S3 and Elastic Beanstalk.

- [Group Docker Images](https://gitlab.com/serost-pet-php-app/group-docker-images). Sources of useful docker containers.<br/>
Creates useful docker containers with preinstalled software.<br/>
This is used for example in [PHP Applications CI pipeline here](https://gitlab.com/serost-pet-php-app/php-application-root/-/blob/b0b2e17b32c8bdb80e1f3544a2938661714d0989/.gitlab-ci.yml#L23).<br/>
Containers are pushed to a GitLabs internal container registry.

If you want to know more, see appropriate directory at the root of this repository.
- [Infrastructure](./infrastructure-overview/)
- [PHP Application](./php-application-overview/)

## Used technologies
- Version Control Tools
   - Git
   - GitLab
      - CI/CD Pipeline
      - Merge Requests
      - Environments
      - Variables
      - Protected Branches

- Public Cloud (AWS)
   - EC2
      - Instances
      - Bastion Host (SSH Tunnel)
      - SSH Keys
   - VPC
      - Public and Private (with NAT Instance) subnets
      - Security Groups
      - Routing
   - S3
      - IaC remote state (managed by Terragrunt)
      - Artifact storage (with Elastic Beanstalk)
   - IAM
      - Role for Infrastructure repo
      - Role for GitLab CI, with very limited permissions
   - RDS
      - MySQL database (1 for each environment)
   - Elastic Beanstalk (for CD)
      - Artifact storage (with S3 bucket)
      - Rolling update
      - PHP Management

- IaC
   - Terragrunt
      - Public Modules (cloudposse and others)
   - Terraform
      - Integration with terragrunt
   - Docker Compose

- Containerization
   - Docker
      - Dockerfile
      - Private Docker registry ([GitLab Hosted](https://gitlab.com/serost-pet-php-app/group-docker-images/container_registry))

- Scripting and programming
   - PHP
      - Backend for a website
      - Security
         - SQL Injection Prevention
         - XSS Prevention
   - Bash
   - SQL Querries (.sql files)

- Database
   - MySQL
      - Databases, Tables, Users
      - Security
         - [Principle of least privilege](https://gitlab.com/serost-pet-php-app/php-application-root/-/blob/b0b2e17b32c8bdb80e1f3544a2938661714d0989/sandbox/mysql/bootstrap.sql#L12-18)

- Other
   - Markdown Language

## Possible infrastructure improvements

- Make bastion host a VPN server, WireGuard or OpenVPN, with IaC tools (Packer + Ansible)
- Route53 Routing with health checks (Certificate assumed)

---

You can redistribute code whatever you like, under [CC0 Public Domain License](./LICENSE).
[https://creativecommons.org/public-domain/cc0/](https://creativecommons.org/public-domain/cc0/)

_Sincerely yours, Serhii_

---

## Helpful links

1. [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/) and
   another one [from github](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#lists)

---
